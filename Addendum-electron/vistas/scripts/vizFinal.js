const {ipcRenderer}= require('electron')
const fs=require('fs');
const {remote} = require('electron');
let dialog = remote.dialog;

const WIN = remote.getCurrentWindow();
let options = {
    //Placeholder 1
    title: "Guardar Factura",
    //Placeholder 2
    defaultPath :remote.app.getPath('desktop'),
    //Placeholder 4
    buttonLabel : "Guardar",
    //Placeholder 3
    filters :[
     {name: 'Archivo XML', extensions: ['xml','txt']}
    ]
}
const $=require('jquery');
let factura=ipcRenderer.sendSync('factura-final','');// Generacion de factura final
$('#vis').text(factura);
$('#sav').click(()=>{
    // Obtener direccion donde guardar
    dialog.showSaveDialog(WIN, options,(dir)=>{
        fs.writeFileSync(dir,factura);
        $('#Modal').modal('show')
    });
});
$('#vol').click(()=>{
    window.location='./AbrirArchivo.html';    
})
$('#home').click(()=>{
    ipcRenderer.sendSync('mover-inicio','');
})