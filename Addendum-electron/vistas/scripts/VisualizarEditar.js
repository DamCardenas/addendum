const {ipcRenderer}= require('electron')
const $=require('jquery');
let formatos=ipcRenderer.sendSync('obtener-formatos', 'Obteniendo Formatos...')
let archivo=ipcRenderer.sendSync('obtener-archivo', 'Obteniendo Archivo...')
const Addenda=require(`../AddendaUtil.js`);
let ui=Addenda.generarUI(archivo.formato_idx);
$('#form').html(ui)
window.setInterval(function (){
    $('#vis').text(Addenda.generarAddenda({formato_idx:archivo.formato_idx,input:obtenerDatos()}));
},500);

$('#vol').click(()=>{
    window.location='./AbrirArchivo.html';
})

$('#sig').click(()=>{

    ipcRenderer.sendSync('enviar-addenda',{addenda:$('#vis').text()} )
    window.location='./vizFinal.html';
})
function obtenerDatos()
{
    let cmps=formatos[archivo.formato_idx].campos;
    let output=[]
    for(let i =0;i<cmps.length;i++)
        output.push($(`#${cmps[i].nombre}`).val());
    return output;
}
if(archivo.estado===true)
{
    let campos=Addenda.obtenerAddenda({cfdi_str:archivo.cfdi_str,formato_idx:archivo.formato_idx});
    console.log(campos);

    let elements=document.querySelectorAll(`.${formatos[archivo.formato_idx].xmlns}`);
    for(let i =0;i<elements.length;i++)
    {
        let c=campos[i].replace(/[\n\t]/g,"");
        console.log(c)
        elements[i].setAttribute('value',c);
    }
}



