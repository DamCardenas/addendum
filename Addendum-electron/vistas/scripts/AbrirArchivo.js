const $=require('jquery');
const fs=require('fs');
const {ipcRenderer}= require('electron')
let result={}

$('#archivo').hide()
$('#vola').click(()=>{
    window.location='./SeleccionarFormato.html'
});
$('#volb').click(()=>{
    $('#drag').show();
    $('#archivo').hide();
});


// Disparador de evento
$('#sig').click(()=>{
    if(result.estatus)
    window.location='./visualizarEditar.html';
});



(function () {
    var holder = document.getElementById('drag');

    holder.ondragover = () => {
        return false;
    };

    holder.ondragleave = () => {
        return false;
    };

    holder.ondragend = () => {
        return false;
    };

    holder.ondrop = (e) => {
        e.preventDefault();
        if(e.dataTransfer.files.length>1)
        {
            console.error('Solo se acepta un solo archivo...');
            return false;
        }
        let archivo_jso=e.dataTransfer.files[0];
        // Resultado de analisis de facturas
        result =ipcRenderer.sendSync('analizar-archivo',{url:archivo_jso.path,nombre:archivo_jso.name,tipo:archivo_jso.type});
        // Validar que hacer en caso de que el archivo tenga un error
        // Si no tiene ningun error activar el boton de siguiente
        $('#drag').hide();
        $('#archivo').show();
        $("#archivo-nombre").html(archivo_jso.name);
        //console.log(archivo_str);
        return false;
    };
})();
