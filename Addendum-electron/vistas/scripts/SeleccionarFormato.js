
const {ipcRenderer}= require('electron')
const $=require('jquery');
$('#alerta').hide();
let formatos=ipcRenderer.sendSync('obtener-formatos', 'Obteniendo Formatos...')
for(let i =0;i<formatos.length;i++)
    $('#formato').append(`<option value="${i}">${formatos[i].nombre}</option>`)

$('#sig').click(()=>{
    if($('#formato').val()!=null)
    {
        ipcRenderer.sendSync('seleccionar-formato', $('#formato').val())
        window.location='AbrirArchivo.html';
    }
    else
        $('#alerta').show();
})