# Addendas


## Instrucciones de Compilacion:

1. Descargar el repositorio.
2. Descargar e instalar nodejs.
3. Agregar nodejs a la variable de entorno PATH.
4. Abrir un simbolo del sistema en el directorio del proyecto
5. Ejecutar los sig comandos:

``` bash
npm install 
npm run ep-windows
# Despues del ultimo comando se generara un directorio llamado release-builds
```

6. Dirigirse al directorio release-builds y ejecutar el archivo .exe

## Necesidades del cliente:
- Generar adendas de manera rapida y sencilla sin tener que editar manualmente una factura en xml


## Requisitos:
- Que el software pueda correr en equipos de gama baja sin ningun problema



## Procesos 

1. Determinar si el CFDI contiene una addenda en especifico:
    
    1. Leer archivo xml del cfdi.
    2. Transformar a objeto json.
    3. leer archivo de configuracion de formatos.
    4. seleccionar el formato correspondiente.
    5. Verificar que el nodo cfdi:Addenda contenga el nodo raiz del formato de addenda especificado.
    6. retornar verdadero si lo anterior lo es, en caso de lo contrario, retornar falso.

2. Generar Formato de Addenda Especifica
    
    1. Leer archivo de configuracion de formatos.
    2. Seleccionar Formato Especificado.
    3. Obtener nombres de campos a llenar especificados por el formato
    4. Obtener valores de los campos referentes al formato.
    5.  Reemplazar tokens de relleno  con los valores obtenidos
    6.  retornar cadena de caracteres resultante
    


## Necesidades del proyecto:
- Leer de documentos xml
- Obtener jerarquia del documento (Path) para saber donde colocar la adenda
    - Eso se puede obtener teniendo una factura electronica actual.
- Obtener una manera para verificar que una factura este bien hecha


## 01/09/2019 
El proyecto es funcional pero necesita ser optimizado y mejorado esteticamente para poder ser digno de un release



## Faltantes:
- Manejador de sistema de errores