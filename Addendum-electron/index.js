const electron = require("electron");
const ipcMain = require("electron").ipcMain;
const dialog=electron.dialog;
const fs=require('fs');
const errores=require('./errores.json');
const formato=require('./formatos.json');
const app= electron.app;
const BrowserWindow = electron.BrowserWindow;
//
var analisis={};
//
var addenda="";
let win;
let debug=false;
var openWindow=(filename,_width,_height,_minWidth=_width,_minHeight=_height,_transparent=false,_frame=true,_aot=false,_max=false)=>{
    win=new BrowserWindow({width:_width,height:_height,minWidth:_minWidth,minHeight:_minHeight,transparent:_transparent,frame:_frame,alwaysOnTop:_aot,webPreferences:{nodeIntegration:true}});
    if(_max)win.maximize();
    if(debug)win.webContents.openDevTools();
    win.removeMenu();
    win.loadURL(`file://${__dirname}/`+filename+`.html`);
    
}

// Lee archivo, guarda copia de estatus  y retorna copia de estatus
ipcMain.on('analizar-archivo',(event,arg)=>{
    let factura={};
    factura.archivo=arg;
    if(arg.tipo!="text/xml")
    {
        factura.error=errores[0];
        analisis={estatus:false,factura:factura}
        event.returnValue=analisis;
        return;
    }
    let facC=require('./factura.controlador');
    factura.estatus=facC.analizarFactura(fs.readFileSync(factura.archivo.url));
    if(factura.estatus.formato.nombre!="ERROR")
    {
        analisis={estatus:true,factura:factura}
        event.returnValue=analisis;
    }
    else
    {   
        factura.error=factura.estatus.formato.error;
        analisis={estatus:false,factura:factura}
        event.returnValue=analisis;
    }
});


ipcMain.on('obtener-factura',(event,arg)=>{
    event.returnValue=analisis;
});

ipcMain.on('obtener-addenda',(event,arg)=>{
    let addenda=require('./AddendaUtil');
    event.returnValue=addenda.obtenerAddenda({cfdi_str:fs.readFileSync(analisis.factura.archivo.url),formato_idx:analisis.factura.estatus.formato.indice});
});

ipcMain.on('generar-addenda',(event,arg)=>{
    let addenda=require('./AddendaUtil');
    event.returnValue=addenda.generarAddenda({formato_idx:arg.formato_idx,input:arg.input});
});

ipcMain.on('guardar-addenda',(event,arg)=>{
    let addenda=require('./AddendaUtil');
    analisis.addenda=addenda.generarAddenda({formato_idx:arg.formato_idx,input:arg.input});
    event.returnValue={};
});


ipcMain.on('obtener-campos',(event,arg)=>{
    event.returnValue=formato[analisis.factura.estatus.formato.indice].campos;
});
ipcMain.on('obtener-script',(event,arg)=>{
    event.returnValue=formato[analisis.factura.estatus.formato.indice].script;
});

ipcMain.on('obtener-error',(evt,arg)=>{
    evt.returnValue=errores[arg];
})




ipcMain.on('factura-final', (event, arg) => {
    let Add=require('./AddendaUtil.js');
    let cfdi_final=Add.agregarAddenda({cfdi_str:fs.readFileSync(analisis.factura.archivo.url),addenda_str:analisis.addenda})
    let options = {
        //Placeholder 1
        title: "Guardar Factura",
        //Placeholder 2
        defaultPath :app.getPath('desktop'),
        //Placeholder 4
        buttonLabel : "Guardar",
        //Placeholder 3
        filters :[
         {name: 'Archivo XML', extensions: ['xml','txt']}
        ]
    }
    dialog.showSaveDialog(win, options,(dir)=>{
        if(dir!="")
            fs.writeFile(dir, cfdi_final, function(err) {
                if(err) 
                    event.returnValue=undefined;
                event.returnValue=true;
            });
        else
            event.returnValue=undefined;
    });
})

ipcMain.on('cerrar-app',(event,arg)=>{
    BrowserWindow.getAllWindows()[0].destroy();
})
app.on("ready",()=>{
    //console.log("app started")
    debug=false;
    openWindow("./ng-build/index",850,680,300,300,true,false);
});

app.on("window-all-closed",()=>{
    app.quit()
});

module.exports={openWindow};
