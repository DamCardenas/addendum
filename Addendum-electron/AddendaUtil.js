const xmljs=require('xml-js')
/**
 *
 *
 * @class AddendaUtil
 */
class AddendaUtil
{

/**
 * @static
 * @param {*} cfdi_str
 * @returns boolean
 * @memberof AddendaUtil
 */
static esFactura(cfdi_str)
    {
        let cfdi_jso=xmljs.xml2js(cfdi_str,{compact: true, ignoreComment: true});
        return cfdi_jso['cfdi:Comprobante'] != undefined;
    }
  /**
   * @static
   * @param {*} {cfdi_str,formato_idx}
   * 
   * cfdi_str <- archivo cfdi cargado en una cadena de caracteres
   * 
   * formato_idx <- indice del arreglo que define a un formato de adenda
   * 
   * @returns boolean
   * @memberof AddendaUtil
   * 
   * 
   */
    static existeAddenda({cfdi_str,formato_idx})
    {
        let formato_jso=require('./formatos.json')[formato_idx];
        let cfdi_jso=xmljs.xml2js(cfdi_str,{compact: true, ignoreComment: true});
        return cfdi_jso['cfdi:Comprobante']['cfdi:Addenda'][`${formato_jso['xmlns']}:${formato_jso['raiz']}`] != undefined;
    }
    
    /**
     *
     *
     * @static
     * @param {string} cfdi_str
     * @description Retorna un valor true en caso de que la etiqueta relacionada con la addenda de una factura tenga algun valor y un valor false en caso de lo contrario
     * @memberof AddendaUtil
     */
    static existeAddendav2(cfdi_str)
    {
        let cfdi_jso=xmljs.xml2js(cfdi_str,{compact: true, ignoreComment: true});
        if(cfdi_jso['cfdi:Comprobante']['cfdi:Addenda'] != undefined)
            return Object.getOwnPropertyNames(cfdi_jso['cfdi:Comprobante']['cfdi:Addenda']).length>0
        else
            return false;
    }

    /**
     *
     *
     * @static
     * @param {string} cfdi_str
     * @returns {*} {nombre,index}
     * @description Busca un formato en base al rfc del receptor de la factura independientemente si existe addenda o no
     * @memberof AddendaUtil
     */
    static detectarFormato(cfdi_str)
    {
        let output={nombre:'Desconocido',indice:-1};
        let formatos_jso=require(`${__dirname}/formatos.json`);
        let fac=xmljs.xml2js(cfdi_str,{compact: true, ignoreComment: true})

        if(Array.isArray(fac['cfdi:Comprobante']))
        {   
            output.nombre="ERROR"
            output.error=require('./errores.json')[2];
            return output;
        }
        let rfc=fac['cfdi:Comprobante']['cfdi:Receptor']['_attributes']['Rfc'];
        for (let index = 0; index < formatos_jso.length; index++) {
               const form=formatos_jso[index];
               if(rfc===form.rfc)
                {
                    output={nombre:form.nombre,indice:index};
                    break;
                }
        }
        return output;
    }


    /**
     * @static
     * @param {*} {formato_idx,input}
     * @returns
     * @memberof AddendaUtil
     */
    static generarAddenda({formato_idx,input})
    {
        var formato_jso=require('./formatos.json')[formato_idx];
        let output=formato_jso.base;
        if(formato_jso.campos.length!=input.length)
            return {err:"Numero de argumentos invalido!..."}
        for(let i =0;i<input.length;i++)
            output=output.replace(`\${${i}}`,input[i])
        return output;
    }


    /**
     *
     *
     * @static
     * @param {*} cfdi_str
     * @returns
     * @memberof AddendaUtil
     */
    static eliminarAddenda(cfdi_str)
    {
        let cfdi_jso=xmljs.xml2js(cfdi_str,{compact: true, ignoreComment: true});
        cfdi_jso['cfdi:Comprobante']['cfdi:Addenda']={};
        return xmljs.js2xml(cfdi_jso,{compact: true, ignoreComment: true});
    }
    /**
     *
     *
     * @static
     * @param {*} {cfdi_str,formato_idx}
     * @returns
     * @memberof AddendaUtil
     */
    static obtenerAddenda({cfdi_str,formato_idx})
    {
        let output=[];
        let cfdi_jso=xmljs.xml2js(cfdi_str,{compact: true, ignoreComment: true});
        let formato_jso=require('./formatos.json')[formato_idx];
        formato_jso.campos.forEach(element => {
            let nodo=cfdi_jso['cfdi:Comprobante']['cfdi:Addenda'][`${formato_jso.xmlns}:${formato_jso.raiz}`];
            for(let i=0;i<element.ruta.length;i++)
                nodo=nodo[`${formato_jso.xmlns}:${element.ruta[i]}`];
            nodo=nodo[`${formato_jso.xmlns}:${element.nombre}`]['_text'].replace(/[\n\t]/g,"");
            output.push(nodo);
        });
        return output
    }
    /**
     *
     *
     * @static
     * @param {*} {cfdi_str,addenda_str}
     * @returns
     * @memberof AddendaUtil
     */
    static agregarAddenda({cfdi_str,addenda_str}){
        let cfdi_jso=xmljs.xml2js(cfdi_str,{compact: true, ignoreComment: true});
        let addenda_jso=xmljs.xml2js(addenda_str,{compact: true, ignoreComment: true});
        cfdi_jso['cfdi:Comprobante']['cfdi:Addenda']=addenda_jso;
        return xmljs.js2xml(cfdi_jso,{compact: true, ignoreComment: true});
    }

    /**
     *
     *
     * @static
     * @param {*} formato_idx
     * @returns
     * @memberof AddendaUtil
     */
    static generarUI(formato_idx)
    {
        let output="";
        let formato_jso=require('./formatos.json')[formato_idx];
        formato_jso.campos.forEach(element => {
            output+=`\n<div class="form-group"><label for="${element.nombre}">${element.nombre}</label><input type="${element.tipo}" class="form-control ${formato_jso.xmlns}" id="${element.nombre}"></div>`
        });
        output+=`\n${formato_jso.script}`
        return output;
    }
}
module.exports=AddendaUtil;