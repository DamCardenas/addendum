(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<app-navbar ></app-navbar>\n\n<!--<factura-estatus></factura-estatus>\n<factura-drag-drop></factura-drag-drop>-->\n<div class=\"container\">\n    <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component/fin/fin.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component/fin/fin.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\" style=\"height: 30vh\"></div>\n<div class=\"row\">\n    <div class=\"col\"></div>\n    <div class=\"col-sm-12  col-md-7 col-lg-6\">\n            <div class=\"card  animated bounceIn fast \">\n                <div class=\"card-body\">\n                    <h5 class=\"card-title\">Fin del proceso!</h5>\n                    <p class=\"card-text\">El proceso ha concluido.</p>\n                </div>\n                <div class=\"card-footer\">\n                    <div class=\"row\">\n                        <div class=\"col\">\n                            <button class=\"btn btn-danger\" type=\"button\" (click)=\"volver()\">volver</button>\n                        </div>\n                        <div class=\"col\">\n                            <button class=\"btn btn-warning\" type=\"button\" (click)=\"reiniciar()\">Volver a Empezar</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n    </div>\n    <div class=\"col\"></div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/addenda-editar/addenda-editar.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/addenda-editar/addenda-editar.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row animated bounceIn\" style=\"margin-top: 1em;\">\n    <div id=\"form\" class=\"col\" style=\"overflow-y: auto;\" >\n        <app-addenda-form ></app-addenda-form>\n    </div>\n    <div  class=\"col\" >\n        <div class=\"row\">\n                <div class=\"col\"><button (click)=\"volver()\" class=\"btn btn-danger btn-block\" >Volver</button></div>\n                <div class=\"col\"><button class=\"btn btn-primary btn-block\" (click)=\"continuar()\">Continuar</button></div>\n        </div>\n        <div class=\"row\">\n            <pre  id=\"vis\" style=\"margin-top: 1em; height: 75vh; max-width: 45vw; background: #E5E5E5;scrollbar-width: thin; overflow-y: thin; \">   \n            </pre>\n        </div>\n    </div>\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/addenda-form/addenda-form.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/addenda-form/addenda-form.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\" *ngFor=\"let campo of campos\" >\n    <label for=\"{{campo.datos.nombre}}\">{{campo.datos.nombre}}</label>\n    <input type=\"{{campo.datos.tipo}}\" value=\"{{campo.valor}}\" class=\"form-control {{campo.datos.xmlns}}\" id=\"{{campo.datos.nombre}}\">\n</div>\n<div id=\"scripts\">\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/factura-drag-drop/factura-drag-drop.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/factura-drag-drop/factura-drag-drop.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n        <div class=\"row\" style=\"height: 20vh\"></div>\n        <div class=\"row\">\n            <div class=\"col\"></div>\n            <div class=\"col-xs-6 col-sm-7  col-md-6 col-lg-6\" style=\"height: 50vh\">\n                <div    (click)=\"fileInput.click()\" DragAndDrop (archivoDroped)=\"analizarRedireccionar($event)\" class=\"card text-white bg-secondary mb-3 animated bounceIn\" style=\"height:100%; border-radius: 20%;\">\n                    <input hidden type=\"file\" #fileInput (change)=\"analizarRedireccionar ($event.target.files)\">\n                    <div class=\"card-body \">\n                        <div class=\"text-center\">\n                            <h4 class=\"card-title\">Arrastra aqui tu factura </h4>\n                        </div>\n                        <div class=\"text-center\">\n                            <img src=\"assets/xml.png\"  style=\"margin-top: 2vh\" alt=\"archivo xml\">\n                        </div>\n                    </div>\n                  </div>\n            </div>\n            <div class=\"col\"></div>\n        </div>\n\n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/factura-estatus/factura-estatus.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/factura-estatus/factura-estatus.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n        <div class=\"row\" style=\"height: 30vh\"></div>\n        <div class=\"row\">\n            <div class=\"col\"></div>\n            <div class=\" col-sm-12  col-md-7 col-lg-6\" style=\"height: 50vh\">\n                <div class=\"card text-white bg-ligth  mb-3 animated  bounceIn\" style=\" width: 100% ; border-radius: 2.5em;\">\n                    <div class=\"card-header \" style=\" border-radius: 2.5em;\">\n                        <div class=\"row\">\n                            <div class=\"col-4 \">\n                                <img src=\"assets/xml.png\"  style=\"width: 80px;height: 80px;\" alt=\"archivo xml\">\n                            </div>\n                            <div class=\"col-7\">\n                                <div class=\"row\">\n                                    <div class=\"text-center\">\n                                        <h2 >{{nombre}}</h2>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <span class=\"badge badge-pill badge-success\" >{{estatus}}</span> \n                                    <span class=\"badge badge-pill badge-warning\" >{{formato}}</span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-body \">\n                        <div class=\"row\">\n                                <div class=\"col\"><button (click)=\"volver()\" class=\"btn btn-danger btn-block\" >Volver</button></div>\n                                <div class=\"col\"><button class=\"btn btn-primary btn-block\" (click)=\"editar()\" *ngIf=\"habilitado\">{{accion}}</button></div>\n                        </div>   \n                    </div>\n                  </div>\n            </div>\n            <div class=\"col\"></div>\n        </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/guardar-factura/guardar-factura.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/guardar-factura/guardar-factura.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\" style=\"height: 20vh\">\n        <div class=\"col\"><button (click)=\"volver()\" class=\"btn btn-danger btn-block\">Volver</button></div>\n</div>\n<div class=\"row\">\n    <div class=\"col\">\n    </div>\n    <div class=\"col-xs-6 col-sm-7  col-md-6 col-lg-6\" style=\"height: 50vh\">\n        <div (click)=\"guardar()\" class=\"card text-white bg-secondary mb-3 animated bounceIn\" style=\"height:100%; border-radius: 20%;\">\n            <div class=\"card-body \">\n                <div class=\"card-title text-center\">\n                    Guardar\n                </div>\n                <div class=\"text-center\">\n                    <img src=\"assets/xml.png\"  style=\"margin-top: 4vh\" alt=\"archivo xml\">\n                </div>\n                <div class=\"card-title text-center\">\n                    {{nombre}}\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"col\"></div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/modal/modal.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/modal/modal.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"row\" style=\"height: 30vh\"></div>\r\n<div class=\"row\">\r\n    <div class=\"col\"></div>\r\n    <div class=\"col-sm-12  col-md-7 col-lg-6\">\r\n            <div class=\"card bg-danger text-white animated shake fast \">\r\n                <div class=\"card-body\">\r\n                    <h5 class=\"card-title\">Error {{codigo}}</h5>\r\n                    <p class=\"card-text\">{{descripcion}}</p>\r\n                </div>\r\n                <div class=\"card-footer\">\r\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"regresar()\">Regresar</button>\r\n                </div>\r\n            </div>\r\n    </div>\r\n    <div class=\"col\"></div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/shared/navbar/navbar.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/shared/navbar/navbar.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-dark sticky-top\">\r\n    <a class=\"navbar-brand\" href=\"#\">\r\n        <img src=\"assets/icon.png\" width=\"25\" height=\"25\" class=\"d-inline-block align-top\" alt=\"\">\r\n        ADDEndum\r\n    </a>\r\n    <div class=\"form-inline \">\r\n      <button class=\"btn\" (click)=\"cerrarApp()\" ><img src=\"assets/exit.png\" width=\"25\" height=\"25\"  alt=\"salir\"></button>\r\n    </div>\r\n\r\n</nav>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/shared/pasos/pasos.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/shared/pasos/pasos.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"progress rounded-pill\" style=\"position: relative;height: 30px; width:80%;\">\n    <div class=\"progress-bar\" role=\"progressbar\" style=\"width: 25%; height: 100%; position: absolute\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n    <div class=\"row rounded-pill\" style=\"left:1.7%; position: absolute; height: 100%; width: 100%;z-index: 1000;\">\n        <div class=\"col border border-secundary\">\n            <div class=\"text-center  text-white\">\n                Primer paso\n            </div>\n        </div>\n        <div class=\"col border border-secundary\"></div>\n        <div class=\"col border border-secundary\"></div>\n        <div class=\"col border border-secundary\"></div>\n        <div class=\"col border border-secundary\"></div>\n        <div class=\"col border border-secundary\"></div>\n        <div class=\"col border border-secundary\"></div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'addendum-angular';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.routes */ "./src/app/app.routes.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/shared/navbar/navbar.component */ "./src/app/components/shared/navbar/navbar.component.ts");
/* harmony import */ var _components_factura_drag_drop_factura_drag_drop_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/factura-drag-drop/factura-drag-drop.component */ "./src/app/components/factura-drag-drop/factura-drag-drop.component.ts");
/* harmony import */ var _components_factura_estatus_factura_estatus_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/factura-estatus/factura-estatus.component */ "./src/app/components/factura-estatus/factura-estatus.component.ts");
/* harmony import */ var _components_shared_pasos_pasos_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/shared/pasos/pasos.component */ "./src/app/components/shared/pasos/pasos.component.ts");
/* harmony import */ var _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");
/* harmony import */ var _directives_drag_and_drop_directive__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./directives/drag-and-drop.directive */ "./src/app/directives/drag-and-drop.directive.ts");
/* harmony import */ var _components_addenda_form_addenda_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/addenda-form/addenda-form.component */ "./src/app/components/addenda-form/addenda-form.component.ts");
/* harmony import */ var _components_addenda_editar_addenda_editar_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/addenda-editar/addenda-editar.component */ "./src/app/components/addenda-editar/addenda-editar.component.ts");
/* harmony import */ var _components_guardar_factura_guardar_factura_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/guardar-factura/guardar-factura.component */ "./src/app/components/guardar-factura/guardar-factura.component.ts");
/* harmony import */ var _component_fin_fin_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./component/fin/fin.component */ "./src/app/component/fin/fin.component.ts");



// Rutas

// Servicios
// Componentes












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"],
                _components_factura_drag_drop_factura_drag_drop_component__WEBPACK_IMPORTED_MODULE_6__["FacturaDragDropComponent"],
                _components_factura_estatus_factura_estatus_component__WEBPACK_IMPORTED_MODULE_7__["FacturaEstatusComponent"],
                _components_shared_pasos_pasos_component__WEBPACK_IMPORTED_MODULE_8__["PasosComponent"],
                _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_9__["ModalComponent"],
                _directives_drag_and_drop_directive__WEBPACK_IMPORTED_MODULE_11__["DragAndDropDirective"],
                _components_addenda_form_addenda_form_component__WEBPACK_IMPORTED_MODULE_12__["AddendaFormComponent"],
                _components_addenda_editar_addenda_editar_component__WEBPACK_IMPORTED_MODULE_13__["AddendaEditarComponent"],
                _components_guardar_factura_guardar_factura_component__WEBPACK_IMPORTED_MODULE_14__["GuardarFacturaComponent"],
                _component_fin_fin_component__WEBPACK_IMPORTED_MODULE_15__["FinComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routes__WEBPACK_IMPORTED_MODULE_3__["app_routing"],
                ngx_electron__WEBPACK_IMPORTED_MODULE_10__["NgxElectronModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routes.ts":
/*!*******************************!*\
  !*** ./src/app/app.routes.ts ***!
  \*******************************/
/*! exports provided: app_routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "app_routing", function() { return app_routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_factura_drag_drop_factura_drag_drop_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/factura-drag-drop/factura-drag-drop.component */ "./src/app/components/factura-drag-drop/factura-drag-drop.component.ts");
/* harmony import */ var _components_factura_estatus_factura_estatus_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/factura-estatus/factura-estatus.component */ "./src/app/components/factura-estatus/factura-estatus.component.ts");
/* harmony import */ var _components_addenda_editar_addenda_editar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/addenda-editar/addenda-editar.component */ "./src/app/components/addenda-editar/addenda-editar.component.ts");
/* harmony import */ var _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _components_guardar_factura_guardar_factura_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/guardar-factura/guardar-factura.component */ "./src/app/components/guardar-factura/guardar-factura.component.ts");
/* harmony import */ var _component_fin_fin_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./component/fin/fin.component */ "./src/app/component/fin/fin.component.ts");







var app_routes = [
    { path: 'cargar_archivo', component: _components_factura_drag_drop_factura_drag_drop_component__WEBPACK_IMPORTED_MODULE_1__["FacturaDragDropComponent"] },
    { path: 'archivo_analizado', component: _components_factura_estatus_factura_estatus_component__WEBPACK_IMPORTED_MODULE_2__["FacturaEstatusComponent"] },
    { path: 'modal_error', component: _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_4__["ModalComponent"] },
    { path: 'addenda_editar', component: _components_addenda_editar_addenda_editar_component__WEBPACK_IMPORTED_MODULE_3__["AddendaEditarComponent"] },
    { path: 'guardar_factura', component: _components_guardar_factura_guardar_factura_component__WEBPACK_IMPORTED_MODULE_5__["GuardarFacturaComponent"] },
    { path: 'fin', component: _component_fin_fin_component__WEBPACK_IMPORTED_MODULE_6__["FinComponent"] },
    { path: '**', pathMatch: 'full', redirectTo: 'cargar_archivo' }
];
var app_routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(app_routes);


/***/ }),

/***/ "./src/app/component/fin/fin.component.css":
/*!*************************************************!*\
  !*** ./src/app/component/fin/fin.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC9maW4vZmluLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/component/fin/fin.component.ts":
/*!************************************************!*\
  !*** ./src/app/component/fin/fin.component.ts ***!
  \************************************************/
/*! exports provided: FinComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinComponent", function() { return FinComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var FinComponent = /** @class */ (function () {
    function FinComponent(_electron, _router) {
        this._electron = _electron;
        this._router = _router;
    }
    FinComponent.prototype.volver = function () {
        this._router.navigateByUrl('/guardar_factura');
    };
    FinComponent.prototype.reiniciar = function () {
        this._router.navigateByUrl('/cargar_archivo');
    };
    FinComponent.prototype.ngOnInit = function () {
    };
    FinComponent.ctorParameters = function () { return [
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_2__["ElectronService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    FinComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fin',
            template: __webpack_require__(/*! raw-loader!./fin.component.html */ "./node_modules/raw-loader/index.js!./src/app/component/fin/fin.component.html"),
            styles: [__webpack_require__(/*! ./fin.component.css */ "./src/app/component/fin/fin.component.css")]
        })
    ], FinComponent);
    return FinComponent;
}());



/***/ }),

/***/ "./src/app/components/addenda-editar/addenda-editar.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/addenda-editar/addenda-editar.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRkZW5kYS1lZGl0YXIvYWRkZW5kYS1lZGl0YXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/addenda-editar/addenda-editar.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/addenda-editar/addenda-editar.component.ts ***!
  \***********************************************************************/
/*! exports provided: AddendaEditarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddendaEditarComponent", function() { return AddendaEditarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AddendaEditarComponent = /** @class */ (function () {
    function AddendaEditarComponent(_router, _electron) {
        this._router = _router;
        this._electron = _electron;
    }
    AddendaEditarComponent.prototype.valores = function () {
        var cmps = this._electron.ipcRenderer.sendSync('obtener-campos', {});
        var output = [];
        for (var i = 0; i < cmps.length; i++)
            output.push(document.querySelector("#" + cmps[i].nombre).value);
        //console.log(output)
        return output;
    };
    AddendaEditarComponent.prototype.volver = function () {
        this._router.navigateByUrl('/archivo_analizado');
    };
    AddendaEditarComponent.prototype.continuar = function () {
        this._electron.ipcRenderer.sendSync('guardar-addenda', { formato_idx: this.respuesta.factura.estatus.formato.indice, input: this.valores() });
        window.clearInterval(this.interval);
        this._router.navigateByUrl('/guardar_factura');
    };
    AddendaEditarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.respuesta = this._electron.ipcRenderer.sendSync('obtener-factura', {});
        //let campos;
        this.interval = window.setInterval(function () {
            document.getElementById('vis').innerText = _this._electron.ipcRenderer.sendSync('generar-addenda', { formato_idx: _this.respuesta.factura.estatus.formato.indice, input: _this.valores() });
        }, 500);
    };
    AddendaEditarComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_2__["ElectronService"] }
    ]; };
    AddendaEditarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-addenda-editar',
            template: __webpack_require__(/*! raw-loader!./addenda-editar.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/addenda-editar/addenda-editar.component.html"),
            styles: [__webpack_require__(/*! ./addenda-editar.component.css */ "./src/app/components/addenda-editar/addenda-editar.component.css")]
        })
    ], AddendaEditarComponent);
    return AddendaEditarComponent;
}());



/***/ }),

/***/ "./src/app/components/addenda-form/addenda-form.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/addenda-form/addenda-form.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRkZW5kYS1mb3JtL2FkZGVuZGEtZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/addenda-form/addenda-form.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/addenda-form/addenda-form.component.ts ***!
  \*******************************************************************/
/*! exports provided: AddendaFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddendaFormComponent", function() { return AddendaFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");




var AddendaFormComponent = /** @class */ (function () {
    function AddendaFormComponent(_electron, _renderer2, _document) {
        this._electron = _electron;
        this._renderer2 = _renderer2;
        this._document = _document;
        this.campos = [];
    }
    AddendaFormComponent.prototype.ngOnInit = function () {
        var cmp = this._electron.ipcRenderer.sendSync('obtener-campos', {});
        var respuesta = this._electron.ipcRenderer.sendSync('obtener-factura', {});
        var cont;
        if (respuesta.factura.estatus.addenda) {
            cont = this._electron.ipcRenderer.sendSync('obtener-addenda', {});
            for (var index = 0; index < cmp.length; index++)
                this.campos.push({ datos: cmp[index], valor: cont[index] });
        }
        else
            for (var index = 0; index < cmp.length; index++)
                this.campos.push({ datos: cmp[index], valor: "" });
    };
    // Se agregan scripts para calculos despues de cargar la vista
    AddendaFormComponent.prototype.ngAfterViewInit = function () {
        var script = this._electron.ipcRenderer.sendSync('obtener-script', {});
        var sc = this._renderer2.createElement('script');
        sc.type = "application/javascript";
        sc.text = script;
        this._renderer2.appendChild(this._document.body, sc);
    };
    AddendaFormComponent.ctorParameters = function () { return [
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_3__["ElectronService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: Document, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DOCUMENT"],] }] }
    ]; };
    AddendaFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-addenda-form',
            template: __webpack_require__(/*! raw-loader!./addenda-form.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/addenda-form/addenda-form.component.html"),
            styles: [__webpack_require__(/*! ./addenda-form.component.css */ "./src/app/components/addenda-form/addenda-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_2__["DOCUMENT"]))
    ], AddendaFormComponent);
    return AddendaFormComponent;
}());



/***/ }),

/***/ "./src/app/components/factura-drag-drop/factura-drag-drop.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/factura-drag-drop/factura-drag-drop.component.ts ***!
  \*****************************************************************************/
/*! exports provided: FacturaDragDropComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FacturaDragDropComponent", function() { return FacturaDragDropComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var FacturaDragDropComponent = /** @class */ (function () {
    function FacturaDragDropComponent(_electron, _router) {
        this._electron = _electron;
        this._router = _router;
    }
    FacturaDragDropComponent.prototype.analizarRedireccionar = function (event) {
        var respuesta = this._electron.ipcRenderer.sendSync('analizar-archivo', { url: event.path, nombre: event.name, tipo: event.type });
        if (respuesta.estatus) {
            this._router.navigateByUrl('/archivo_analizado');
        }
        else {
            this._router.navigateByUrl("/modal_error");
        }
    };
    FacturaDragDropComponent.ctorParameters = function () { return [
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_2__["ElectronService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    FacturaDragDropComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'factura-drag-drop',
            template: __webpack_require__(/*! raw-loader!./factura-drag-drop.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/factura-drag-drop/factura-drag-drop.component.html")
        })
    ], FacturaDragDropComponent);
    return FacturaDragDropComponent;
}());



/***/ }),

/***/ "./src/app/components/factura-estatus/factura-estatus.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/factura-estatus/factura-estatus.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button{\r\n    border-radius: .9em;\r\n}\r\n.card{\r\n    background: #E5E5E5;\r\n}\r\n.card-header\r\n{\r\n    background: #949494;\r\n}\r\n*{\r\n    user-select: none; /* supported by Chrome and Opera */\r\n   -webkit-user-select: none; /* Safari */\r\n   -khtml-user-select: none; /* Konqueror HTML */\r\n   -moz-user-select: none; /* Firefox */\r\n   -ms-user-select: none; /* Internet Explorer/Edge */\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mYWN0dXJhLWVzdGF0dXMvZmFjdHVyYS1lc3RhdHVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLG1CQUFtQjtBQUN2QjtBQUNBOztJQUVJLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksaUJBQWlCLEVBQUUsa0NBQWtDO0dBQ3RELHlCQUF5QixFQUFFLFdBQVc7R0FDdEMsd0JBQXdCLEVBQUUsbUJBQW1CO0dBQzdDLHNCQUFzQixFQUFFLFlBQVk7R0FDcEMscUJBQXFCLEVBQUUsMkJBQTJCO0FBQ3JEIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9mYWN0dXJhLWVzdGF0dXMvZmFjdHVyYS1lc3RhdHVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJidXR0b257XHJcbiAgICBib3JkZXItcmFkaXVzOiAuOWVtO1xyXG59XHJcbi5jYXJke1xyXG4gICAgYmFja2dyb3VuZDogI0U1RTVFNTtcclxufVxyXG4uY2FyZC1oZWFkZXJcclxue1xyXG4gICAgYmFja2dyb3VuZDogIzk0OTQ5NDtcclxufVxyXG4qe1xyXG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7IC8qIHN1cHBvcnRlZCBieSBDaHJvbWUgYW5kIE9wZXJhICovXHJcbiAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIFNhZmFyaSAqL1xyXG4gICAta2h0bWwtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEtvbnF1ZXJvciBIVE1MICovXHJcbiAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEZpcmVmb3ggKi9cclxuICAgLW1zLXVzZXItc2VsZWN0OiBub25lOyAvKiBJbnRlcm5ldCBFeHBsb3Jlci9FZGdlICovXHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/factura-estatus/factura-estatus.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/factura-estatus/factura-estatus.component.ts ***!
  \*************************************************************************/
/*! exports provided: FacturaEstatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FacturaEstatusComponent", function() { return FacturaEstatusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var FacturaEstatusComponent = /** @class */ (function () {
    function FacturaEstatusComponent(_electron, _router) {
        this._electron = _electron;
        this._router = _router;
    }
    FacturaEstatusComponent.prototype.ngOnInit = function () {
        var respuesta = this._electron.ipcRenderer.sendSync('obtener-factura', {});
        this.nombre = respuesta.factura.archivo.nombre;
        this.estatus = respuesta.factura.estatus.addenda ? "Con Addenda" : "";
        this.formato = respuesta.factura.estatus.formato.nombre === 'Desconocido' ? "Formato Desconocido" : respuesta.factura.estatus.formato.nombre;
        this.accion = respuesta.factura.estatus.addenda ? 'Editar' : 'Addendar';
        this.habilitado = respuesta.factura.estatus.formato.nombre != 'Desconocido';
    };
    FacturaEstatusComponent.prototype.volver = function () {
        this._router.navigateByUrl('/cargar_archivo');
    };
    FacturaEstatusComponent.prototype.editar = function () {
        this._router.navigateByUrl('/addenda_editar');
    };
    FacturaEstatusComponent.ctorParameters = function () { return [
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_2__["ElectronService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    FacturaEstatusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'factura-estatus',
            template: __webpack_require__(/*! raw-loader!./factura-estatus.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/factura-estatus/factura-estatus.component.html"),
            styles: [__webpack_require__(/*! ./factura-estatus.component.css */ "./src/app/components/factura-estatus/factura-estatus.component.css")]
        })
    ], FacturaEstatusComponent);
    return FacturaEstatusComponent;
}());



/***/ }),

/***/ "./src/app/components/guardar-factura/guardar-factura.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/guardar-factura/guardar-factura.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZ3VhcmRhci1mYWN0dXJhL2d1YXJkYXItZmFjdHVyYS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/guardar-factura/guardar-factura.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/guardar-factura/guardar-factura.component.ts ***!
  \*************************************************************************/
/*! exports provided: GuardarFacturaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuardarFacturaComponent", function() { return GuardarFacturaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




//import { Base64_} from "js-base64";
//import { toBase64String } from '@angular/compiler/src/output/source_map';
var GuardarFacturaComponent = /** @class */ (function () {
    function GuardarFacturaComponent(_router, _electron) {
        this._router = _router;
        this._electron = _electron;
    }
    GuardarFacturaComponent.prototype.volver = function () {
        this._router.navigateByUrl('/addenda_editar');
    };
    GuardarFacturaComponent.prototype.guardar = function () {
        if (this._electron.ipcRenderer.sendSync('factura-final', this.nombre))
            this._router.navigateByUrl('/fin');
    };
    GuardarFacturaComponent.prototype.ngOnInit = function () {
        var respuesta = this._electron.ipcRenderer.sendSync('obtener-factura', {});
        this.nombre = "factura_" + respuesta.factura.archivo.nombre;
        //console.log(ff);
        //this.uri=this._base64.encode(ff);
        //console.log(atob());
    };
    GuardarFacturaComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_2__["ElectronService"] }
    ]; };
    GuardarFacturaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-guardar-factura',
            template: __webpack_require__(/*! raw-loader!./guardar-factura.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/guardar-factura/guardar-factura.component.html"),
            styles: [__webpack_require__(/*! ./guardar-factura.component.css */ "./src/app/components/guardar-factura/guardar-factura.component.css")]
        })
    ], GuardarFacturaComponent);
    return GuardarFacturaComponent;
}());



/***/ }),

/***/ "./src/app/components/modal/modal.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/modal/modal.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwvbW9kYWwuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/modal/modal.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/modal/modal.component.ts ***!
  \*****************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");




var ModalComponent = /** @class */ (function () {
    function ModalComponent(_router, _electron) {
        this._router = _router;
        this._electron = _electron;
    }
    ModalComponent.prototype.regresar = function () {
        this._router.navigateByUrl('/cargar_archivo');
    };
    ModalComponent.prototype.ngOnInit = function () {
        var respuesta = this._electron.ipcRenderer.sendSync('obtener-factura', {});
        this.codigo = respuesta.factura.error.codigo;
        this.descripcion = respuesta.factura.error.descripcion.replace('${archivo}', respuesta.factura.archivo.nombre);
    };
    ModalComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_3__["ElectronService"] }
    ]; };
    ModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! raw-loader!./modal.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/modal/modal.component.html"),
            styles: [__webpack_require__(/*! ./modal.component.css */ "./src/app/components/modal/modal.component.css")]
        })
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/components/shared/navbar/navbar.component.css":
/*!***************************************************************!*\
  !*** ./src/app/components/shared/navbar/navbar.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav{\r\n    background: #949494;\r\n\r\n}\r\n.navbar {\r\n    -webkit-user-select: none;\r\n    -webkit-app-region: drag;\r\n  }\r\nbutton {\r\n    -webkit-app-region: no-drag;\r\n  }\r\n*{\r\n    user-select: none; /* supported by Chrome and Opera */\r\n   -webkit-user-select: none; /* Safari */\r\n   -khtml-user-select: none; /* Konqueror HTML */\r\n   -moz-user-select: none; /* Firefox */\r\n   -ms-user-select: none; /* Internet Explorer/Edge */\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zaGFyZWQvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUJBQW1COztBQUV2QjtBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLHdCQUF3QjtFQUMxQjtBQUVBO0lBQ0UsMkJBQTJCO0VBQzdCO0FBQ0Y7SUFDSSxpQkFBaUIsRUFBRSxrQ0FBa0M7R0FDdEQseUJBQXlCLEVBQUUsV0FBVztHQUN0Qyx3QkFBd0IsRUFBRSxtQkFBbUI7R0FDN0Msc0JBQXNCLEVBQUUsWUFBWTtHQUNwQyxxQkFBcUIsRUFBRSwyQkFBMkI7QUFDckQiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3NoYXJlZC9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJuYXZ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjOTQ5NDk0O1xyXG5cclxufVxyXG4ubmF2YmFyIHtcclxuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAtd2Via2l0LWFwcC1yZWdpb246IGRyYWc7XHJcbiAgfVxyXG4gIFxyXG4gIGJ1dHRvbiB7XHJcbiAgICAtd2Via2l0LWFwcC1yZWdpb246IG5vLWRyYWc7XHJcbiAgfVxyXG4qe1xyXG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7IC8qIHN1cHBvcnRlZCBieSBDaHJvbWUgYW5kIE9wZXJhICovXHJcbiAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIFNhZmFyaSAqL1xyXG4gICAta2h0bWwtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEtvbnF1ZXJvciBIVE1MICovXHJcbiAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEZpcmVmb3ggKi9cclxuICAgLW1zLXVzZXItc2VsZWN0OiBub25lOyAvKiBJbnRlcm5ldCBFeHBsb3Jlci9FZGdlICovXHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/shared/navbar/navbar.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/shared/navbar/navbar.component.ts ***!
  \**************************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_electron__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-electron */ "./node_modules/ngx-electron/fesm5/ngx-electron.js");



var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(_electron) {
        this._electron = _electron;
    }
    NavbarComponent.prototype.cerrarApp = function () {
        this._electron.ipcRenderer.send('cerrar-app', {});
    };
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.ctorParameters = function () { return [
        { type: ngx_electron__WEBPACK_IMPORTED_MODULE_2__["ElectronService"] }
    ]; };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/shared/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/components/shared/navbar/navbar.component.css")]
        })
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/shared/pasos/pasos.component.css":
/*!*************************************************************!*\
  !*** ./src/app/components/shared/pasos/pasos.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hhcmVkL3Bhc29zL3Bhc29zLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/shared/pasos/pasos.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/shared/pasos/pasos.component.ts ***!
  \************************************************************/
/*! exports provided: PasosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasosComponent", function() { return PasosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PasosComponent = /** @class */ (function () {
    function PasosComponent() {
    }
    PasosComponent.prototype.ngOnInit = function () {
    };
    PasosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'pasos',
            template: __webpack_require__(/*! raw-loader!./pasos.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/shared/pasos/pasos.component.html"),
            styles: [__webpack_require__(/*! ./pasos.component.css */ "./src/app/components/shared/pasos/pasos.component.css")]
        })
    ], PasosComponent);
    return PasosComponent;
}());



/***/ }),

/***/ "./src/app/directives/drag-and-drop.directive.ts":
/*!*******************************************************!*\
  !*** ./src/app/directives/drag-and-drop.directive.ts ***!
  \*******************************************************/
/*! exports provided: DragAndDropDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DragAndDropDirective", function() { return DragAndDropDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DragAndDropDirective = /** @class */ (function () {
    function DragAndDropDirective() {
        this.archivoDroped = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    //Dragover listener
    DragAndDropDirective.prototype.onDragOver = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    //Dragleave listener
    DragAndDropDirective.prototype.onDragLeave = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    //Drop listener
    DragAndDropDirective.prototype.ondrop = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var files = evt.dataTransfer.files;
        if (files.length > 0 && files.length < 2)
            this.archivoDroped.emit(files[0]);
        else
            this.archivoDroped.emit({ err: 'Faltan o sobran archivos...' });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], DragAndDropDirective.prototype, "archivoDroped", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('dragover', ['$event'])
    ], DragAndDropDirective.prototype, "onDragOver", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('dragleave', ['$event'])
    ], DragAndDropDirective.prototype, "onDragLeave", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('drop', ['$event'])
    ], DragAndDropDirective.prototype, "ondrop", null);
    DragAndDropDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[DragAndDrop]'
        })
    ], DragAndDropDirective);
    return DragAndDropDirective;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Proyectos\Addendas\Addendum-angular\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map