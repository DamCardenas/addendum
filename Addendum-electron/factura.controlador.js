const Addenda = require('./AddendaUtil');
const errores=require('./errores.json');
class controladorFactura
{
    static analizarFactura(cfdi_str)
    {
        let output={};
        if(Addenda.esFactura(cfdi_str))
        {
            output.addenda=Addenda.existeAddendav2(cfdi_str);
            output.formato=Addenda.detectarFormato(cfdi_str);
        }
        else
            output={formato:{nombre:"ERROR",error:errores[1]}};
        return output;
    }
}
module.exports=controladorFactura;