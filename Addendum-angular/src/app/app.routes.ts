import { RouterModule,Routes } from "@angular/router";
import {FacturaDragDropComponent} from "./components/factura-drag-drop/factura-drag-drop.component";
import {FacturaEstatusComponent} from "./components/factura-estatus/factura-estatus.component";
import {AddendaEditarComponent} from "./components/addenda-editar/addenda-editar.component";
import { ModalComponent } from "./components/modal/modal.component";
import { GuardarFacturaComponent } from "./components/guardar-factura/guardar-factura.component";
import { FinComponent } from "./component/fin/fin.component";
const app_routes:Routes=[
{path:'cargar_archivo',component:FacturaDragDropComponent},
{path:'archivo_analizado',component:FacturaEstatusComponent},
{path:'modal_error',component:ModalComponent},
{path:'addenda_editar',component:AddendaEditarComponent},
{path:'guardar_factura',component:GuardarFacturaComponent},
{path:'fin',component:FinComponent},
{path:'**',pathMatch:'full',redirectTo:'cargar_archivo'}
]

export const app_routing=RouterModule.forRoot(app_routes);  
