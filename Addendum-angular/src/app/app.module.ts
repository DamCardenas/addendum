import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Rutas
import { app_routing } from "./app.routes";

// Servicios

// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FacturaDragDropComponent } from './components/factura-drag-drop/factura-drag-drop.component';
import { FacturaEstatusComponent } from './components/factura-estatus/factura-estatus.component';
import { PasosComponent } from './components/shared/pasos/pasos.component';
import { ModalComponent } from './components/modal/modal.component';
import { NgxElectronModule } from "ngx-electron";
import { DragAndDropDirective } from './directives/drag-and-drop.directive';
import { AddendaFormComponent } from './components/addenda-form/addenda-form.component';
import { AddendaEditarComponent } from './components/addenda-editar/addenda-editar.component';
import { GuardarFacturaComponent } from './components/guardar-factura/guardar-factura.component';
import { FinComponent } from './component/fin/fin.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FacturaDragDropComponent,
    FacturaEstatusComponent,
    PasosComponent,
    ModalComponent,
    DragAndDropDirective,
    AddendaFormComponent,
    AddendaEditarComponent,
    GuardarFacturaComponent,
    FinComponent
  ],
  imports: [
    BrowserModule,
    app_routing,
    NgxElectronModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
