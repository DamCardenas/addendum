import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fin',
  templateUrl: './fin.component.html',
  styleUrls: ['./fin.component.css']
})
export class FinComponent implements OnInit {
  
  constructor(private _electron:ElectronService,private _router:Router) { }
  volver()
  {
    this._router.navigateByUrl('/guardar_factura');
  }
  reiniciar()
  {
    this._router.navigateByUrl('/cargar_archivo');
  }
  ngOnInit() {
  }

}
