import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router"
import { ElectronService } from "ngx-electron";


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  private descripcion:string;
  private codigo:number;
  constructor(private _router: Router,private _electron:ElectronService) { }
  regresar()
  {
    this._router.navigateByUrl('/cargar_archivo');
  }
  ngOnInit() {
    let respuesta=this._electron.ipcRenderer.sendSync('obtener-factura',{});

    this.codigo=respuesta.factura.error.codigo;
    this.descripcion=respuesta.factura.error.descripcion.replace('${archivo}',respuesta.factura.archivo.nombre);
  }

}
