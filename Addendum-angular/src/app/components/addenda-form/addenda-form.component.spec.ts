import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddendaFormComponent } from './addenda-form.component';

describe('AddendaFormComponent', () => {
  let component: AddendaFormComponent;
  let fixture: ComponentFixture<AddendaFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddendaFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
