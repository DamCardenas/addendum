import { Component, OnInit,AfterViewInit,Inject,Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ElectronService } from 'ngx-electron';


@Component({
  selector: 'app-addenda-form',
  templateUrl: './addenda-form.component.html',
  styleUrls: ['./addenda-form.component.css']
})
export class AddendaFormComponent implements OnInit,AfterViewInit {

  private campos:any[]=[];
  constructor(private _electron:ElectronService,private _renderer2: Renderer2, 
    @Inject(DOCUMENT) private _document: Document) { }

  
  ngOnInit() {
    let cmp=this._electron.ipcRenderer.sendSync('obtener-campos',{})
    let respuesta=this._electron.ipcRenderer.sendSync('obtener-factura',{});
   
    let cont;
    if(respuesta.factura.estatus.addenda)
    {
      cont=this._electron.ipcRenderer.sendSync('obtener-addenda',{});
      for (let index = 0; index < cmp.length; index++)
        this.campos.push({datos:cmp[index],valor:cont[index]});
    }
    else
      for (let index = 0; index < cmp.length; index++)
        this.campos.push({datos:cmp[index],valor:""});
  }

  // Se agregan scripts para calculos despues de cargar la vista
  ngAfterViewInit(){
    let script=this._electron.ipcRenderer.sendSync('obtener-script',{});
    let sc = this._renderer2.createElement('script');
    sc.type = `application/javascript`;
    sc.text = script;
    this._renderer2.appendChild(this._document.body, sc);
  }

}
