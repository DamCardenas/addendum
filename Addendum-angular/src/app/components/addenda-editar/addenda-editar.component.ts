import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Router } from '@angular/router';
@Component({
  selector: 'app-addenda-editar',
  templateUrl: './addenda-editar.component.html',
  styleUrls: ['./addenda-editar.component.css']
})
export class AddendaEditarComponent implements OnInit {
  private respuesta:any;
  private interval:any;
  constructor(private _router:Router,private _electron:ElectronService) { }
  valores()
  {
    let cmps=this._electron.ipcRenderer.sendSync('obtener-campos',{});
    let output=[]
    for(let i =0;i<cmps.length;i++)
        output.push((<HTMLInputElement>document.querySelector(`#${cmps[i].nombre}`)).value);
    //console.log(output)
    return output;
  }
  volver()
  {
    this._router.navigateByUrl('/archivo_analizado');
  }
  continuar()
  {
    this._electron.ipcRenderer.sendSync('guardar-addenda',{formato_idx:this.respuesta.factura.estatus.formato.indice,input:this.valores()});
    window.clearInterval(this.interval);
    this._router.navigateByUrl('/guardar_factura');
  }
  ngOnInit() {
    this.respuesta=this._electron.ipcRenderer.sendSync('obtener-factura',{});
    //let campos;
    this.interval=window.setInterval(()=>{
      document.getElementById('vis').innerText=this._electron.ipcRenderer.sendSync('generar-addenda',{formato_idx:this.respuesta.factura.estatus.formato.indice,input:this.valores()});
    },500);
  
  }

}
