import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddendaEditarComponent } from './addenda-editar.component';

describe('AddendaEditarComponent', () => {
  let component: AddendaEditarComponent;
  let fixture: ComponentFixture<AddendaEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddendaEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddendaEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
