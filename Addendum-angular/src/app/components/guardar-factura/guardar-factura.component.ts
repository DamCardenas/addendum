import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Router } from '@angular/router';
//import { Base64_} from "js-base64";
//import { toBase64String } from '@angular/compiler/src/output/source_map';

@Component({
  selector: 'app-guardar-factura',
  templateUrl: './guardar-factura.component.html',
  styleUrls: ['./guardar-factura.component.css']
})
export class GuardarFacturaComponent implements OnInit {

  private nombre:string;
  private uri:string;
  
  constructor(private _router:Router,private _electron:ElectronService) { }

  volver()
  {
    this._router.navigateByUrl('/addenda_editar');
  }
  guardar()
  {
    if(this._electron.ipcRenderer.sendSync('factura-final',this.nombre))
      this._router.navigateByUrl('/fin');
  }
  ngOnInit() {
    let respuesta=this._electron.ipcRenderer.sendSync('obtener-factura',{});
    this.nombre=`factura_${respuesta.factura.archivo.nombre}`
    //console.log(ff);
    //this.uri=this._base64.encode(ff);
    //console.log(atob());
  }

}
