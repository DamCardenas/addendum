import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardarFacturaComponent } from './guardar-factura.component';

describe('GuardarFacturaComponent', () => {
  let component: GuardarFacturaComponent;
  let fixture: ComponentFixture<GuardarFacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardarFacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardarFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
