import { Component } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Router } from '@angular/router';

@Component({
  selector: 'factura-drag-drop',
  templateUrl: './factura-drag-drop.component.html'
})
export class FacturaDragDropComponent  {
  
  
  constructor(private _electron:ElectronService,private _router:Router){}


  analizarRedireccionar(event) {
    let respuesta=this._electron.ipcRenderer.sendSync('analizar-archivo',{url:event.path,nombre:event.name,tipo:event.type})
    if(respuesta.estatus)
    {
      this._router.navigateByUrl('/archivo_analizado');
    }
    else
    {
      this._router.navigateByUrl(`/modal_error`);
    }
  }

}
