import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Router } from '@angular/router';

@Component({
  selector: 'factura-estatus',
  templateUrl: './factura-estatus.component.html',
  styleUrls: ['./factura-estatus.component.css']
})
export class FacturaEstatusComponent implements OnInit {
  private nombre:string;
  private estatus:string;
  private formato:string;
  private error:string;
  private accion:string;
  private habilitado:boolean;
  constructor(private _electron:ElectronService,private _router:Router) { }

  ngOnInit() {
    let respuesta=this._electron.ipcRenderer.sendSync('obtener-factura',{});
    this.nombre=respuesta.factura.archivo.nombre;
    this.estatus=respuesta.factura.estatus.addenda?"Con Addenda":"";
    this.formato=respuesta.factura.estatus.formato.nombre==='Desconocido'?"Formato Desconocido":respuesta.factura.estatus.formato.nombre;
    this.accion =respuesta.factura.estatus.addenda?'Editar':'Addendar';
    this.habilitado=respuesta.factura.estatus.formato.nombre!='Desconocido';
  }
  volver()
  {
    this._router.navigateByUrl('/cargar_archivo');
  }
  editar()
  {
    this._router.navigateByUrl('/addenda_editar');
  }

}
