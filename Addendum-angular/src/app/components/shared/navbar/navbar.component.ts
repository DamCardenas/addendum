import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  

  constructor(private _electron:ElectronService) {}
  
  cerrarApp()
  {
    this._electron.ipcRenderer.send('cerrar-app',{})
  }

  ngOnInit() {
  }

}
